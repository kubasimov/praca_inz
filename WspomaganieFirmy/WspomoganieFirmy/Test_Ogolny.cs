﻿using NUnit.Framework;

namespace WspomoganieFirmy
{

    [TestFixture]
    public class Test_Ogolny
    {
        [Test]
        public void Create_blank_dokument()
        {
            Dokument dokument = new Dokument();
            
            Assert.IsInstanceOf<Dokument>(dokument);
            

        }

        [Test]
        public void Add_Subject_to_dokument()
        {
            Dokument dokument = new Dokument();

            dokument.Tytul = "Umowa z dnia 12/13/15";

            Assert.AreEqual("Umowa z dnia 12/13/15",dokument.Tytul);
        }

        [Test]
        public void Create_Specific_dokument_as_Umowa()
        {
            Dokument Umowa = new Dokument(Rodzaj.Umowa);

            Assert.AreEqual("Umowa",Umowa.Typ);
        }

        [Test]
        public void Add_Date_to_dokument()
        {
            
        }
    }

    public class Dokument

    {
        private string tytul;
        private Rodzaj rodzaj;

        public Dokument()
        {
        }

        public Dokument(Rodzaj umowa)
        { 
            switch ((int)umowa)
            {
                case 1:
                    rodzaj=Rodzaj.Umowa;
                    break;
                    default:
                    rodzaj=Rodzaj.Zamówienie;
                    break;

            }


        }

        public string Tytul
        {
            get { return tytul; }
            set
            {
                if (value!="") 
                    tytul = value;
                else
                {
                    tytul = "Wpisz nazwe dokumentu";
                }
            }
        }

        public string Typ
        {
            get { return rodzaj.ToString(); }
        }
    }
}