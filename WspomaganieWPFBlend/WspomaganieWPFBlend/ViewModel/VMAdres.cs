﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using WspomaganieWPFBlend.Annotations;
using Wspomaganie_Obiegu_Dokumentow.Collections;
using Wspomaganie_Obiegu_Dokumentow.DataBase;
using Wspomaganie_Obiegu_Dokumentow.Model;

namespace Wspomaganie_Obiegu_Dokumentow.ViewModel 
{
    public class VMAdres :INotifyPropertyChanged
    {
        #region zmienne
        private int _id ;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id=value;
                OnPropertyChanged();
            }
        }
      
        private string _ulica;
        public string Ulica
        {
            get
            {
                return _ulica;
            }
            set
            {
                if (_ulica != value)
                {
                    _ulica=value;
                    OnPropertyChanged();
                }
                
            }
        }

        private string _dom;
        public string Dom
        {
            get
            {
                return _dom;
            }
            set
            {
                if (_dom != value)
                {
                    _dom = value;
                    OnPropertyChanged();
                }

            }
        }

        private string _lokal;
        public string Lokal
        {
            get
            {
                return _lokal;
            }
            set
            {
                if (_lokal != value)
                {
                    _lokal = value;
                    OnPropertyChanged();
                }

            }
        }

        private string _kod;
        public string Kod
        {
            get
            {
                return _kod;
            }
            set
            {
                if (_kod != value)
                {
                    _kod = value;
                    OnPropertyChanged();
                }

            }
        }

        private string _miejscowosc;
        public string Miejscowosc
        {
            get
            {
                return _miejscowosc;
            }
            set
            {
                if (_miejscowosc != value)
                {
                    _miejscowosc = value;
                    OnPropertyChanged();
                }

            }
        }

        private int _wojewodztwo;
        public int Wojewodztwo
        {
            get
            {
                return _wojewodztwo;
            }
            set
            {
                if (_wojewodztwo != value)
                {
                    _wojewodztwo = value;
                    OnPropertyChanged();
                }

            }
        }

        private CWojewodztwa _wojewodztwa;
        public CWojewodztwa Wojewodztwa
        {
            get
            {
                return _wojewodztwa;
            }
            set
            {
                if (_wojewodztwa != value)
                {
                    _wojewodztwa = value;
                    OnPropertyChanged();
                }
            }
        }
        
        #endregion

         public VMAdres()
        {
// ReSharper disable ObjectCreationAsStatement
             new LoadDataOnStartProgram();
// ReSharper restore ObjectCreationAsStatement


             var ab = new AccessBase();

             var t = ab.GetById<MAdres>(2);

            
             _ulica = t.Ulica;
             _dom = t.Dom;
             _lokal = t.Lokal;
             _kod = t.Kod;
             _miejscowosc = t.Miejscowosc;
             _wojewodztwo = t.Wojewodztwo.Id-1;

             
             
             var er = new CWojewodztwa();

             _wojewodztwa = er;


        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));

        }
    }

    
}