﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WspomaganieWPFBlend.Annotations;
using Wspomaganie_Obiegu_Dokumentow.DataBase;
using Wspomaganie_Obiegu_Dokumentow.Model;

namespace Wspomaganie_Obiegu_Dokumentow.ViewModel
{
    class VMMain:INotifyPropertyChanged
    {
        private ObservableCollection<MZamowienie> _zamowienia;
        public ObservableCollection<MZamowienie> Zamowienia
        {
            get
            {
                return _zamowienia;
            }
            set
            {
                if (_zamowienia != value)
                {
                    _zamowienia = value;
                    OnPropertyChanged();
                }
            }
        }

        private ObservableCollection<MOsoba> _klienciPrywatni;
        public ObservableCollection<MOsoba> Klienci
        {
            get { return _klienciPrywatni; }
            set
            {
                if (_klienciPrywatni != value)
                {
                    _klienciPrywatni = value;
                    OnPropertyChanged();
                }
            }
        }

        public VMMain()
        {
            new LoadDataOnStartProgram();
            var _accessBase = new AccessBase();
            var zam = _accessBase.GetListByModel<MZamowienie>();
            Zamowienia=new ObservableCollection<MZamowienie>(zam);
            var mOsobas = _accessBase.GetListByNameB<MOsoba>("Firma", false);
            Klienci=new ObservableCollection<MOsoba>(mOsobas);
        }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
