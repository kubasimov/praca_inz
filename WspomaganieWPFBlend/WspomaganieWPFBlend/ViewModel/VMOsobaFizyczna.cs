﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using WspomaganieWPFBlend.Annotations;
using Wspomaganie_Obiegu_Dokumentow.DataBase;
using Wspomaganie_Obiegu_Dokumentow.Model;

namespace Wspomaganie_Obiegu_Dokumentow.ViewModel
{
    class VMOsobaFizyczna:INotifyPropertyChanged
    {

        private readonly AccessBase _accessBase = new AccessBase();

        private MOsoba _osoba;
        public MOsoba Osoba
        {
            get { return _osoba; }
            set
            {
                if (_osoba != value)
                {
                    _osoba = value;
                    OnPropertyChanged();
                }
            }
        }
        
        
        
        public VMOsobaFizyczna()
        {
            _osoba = _accessBase.GetById<MOsoba>(2);
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
