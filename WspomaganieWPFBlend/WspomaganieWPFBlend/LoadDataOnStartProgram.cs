// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System;
using System.Collections.Generic;
using System.Linq;
using Wspomaganie_Obiegu_Dokumentow.DataBase;
using Wspomaganie_Obiegu_Dokumentow.Model;
using Wspomaganie_Obiegu_Dokumentow.SampleData;

namespace Wspomaganie_Obiegu_Dokumentow
{
    class LoadDataOnStartProgram
    {
        private readonly AccessDataBase _accessDataBase = new AccessDataBase();
        private readonly AccessBase _accessBase = new AccessBase();

        public LoadDataOnStartProgram()
        {
            ////ustawienie  bazy
            //_accessDataBase.FirstOpenConnection();
            //_accessDataBase.CloseConnection();



            //var wojewodztwa = new DataWojewodztwa().Wojewodztwa();

            //foreach (var a in from object wojew in wojewodztwa select new MWojewodztwo { Nazwa = wojew.ToString() })
            //{
            //    _accessBase.Save(a);
            //}

            ////wstawienie adresow

            //var adr1 = new DataAdres().Adres1();
            //var adr2 = new DataAdres().Adres2();
            //var adr3 = new DataAdres().Adres3();
            //var adr4 = new DataAdres().Adres4();
            //_accessBase.Save(adr1);
            //_accessBase.Save(adr2);
            //_accessBase.Save(adr3);
            //_accessBase.Save(adr4);



            //var firma = new MOsoba()
            //    {
            //        NazwaPelna = "Pracownia Stolarska Buratino",
            //        NazwaSkrocona = "Buratino",
            //        Nip = 1230857118,
            //        Opis = "Firma stolarska",
            //        Regon = 1490853684,
            //        Www = "www.stolarniaburatino.pl",
            //        Adres = adr2,
            //        Email = new List<string> { "stolarnia.buratino@gmail.com", "projekty.buratino@gmail.com" },
            //        Telefon = new List<string> { "506038383", "506063778" },
            //        Firma = true
            //    };

            //_accessBase.Save(firma);

            //var os = new MOsoba()
            //    {
            //        Imie = "Piotr",
            //        Nazwisko = "Wachowicz",
            //        Adres = adr1,
            //        Avatar = "avatar.jpg",
            //        Email = new List<string> { "kubux@gamil.com", "kubasimov@gmail.com" },
            //        Firma = false,
            //        NrDowodu = "ARU256874",
            //        Opis = "Programista",
            //        Pseudonim = "Kubica",
            //        Tagi = new List<string> { "Kubica", "Kubux", "Kubus" },
            //        Telefon = new List<string> { "506035383" },
            //        Www = "www.KubAsimov.pl"
            //    };
            //_accessBase.Save(os);


            //var faktura = new MFaktura
            //    {
            //        Klient = os,
            //        Numer = "5554684",
            //        Przelew = true,
            //        Sprzedawca = firma,
            //        TerminZaplaty = DateTime.Now,
            //        Tytulem = "Faktura za zabawki",
            //        Wartosc = 25.46,
            //        DataSprzedazy = DateTime.Now,
            //        DataWystawienia = DateTime.Now
            //    };

            //var p = new MProdukt { Ilosc = 35.00, Nazwa = "szafka", Vat = 8, CenaNetto = 12.88 };

            //var p1 = new MProdukt { Ilosc = 1.00, Nazwa = "Kuchnia", Vat = 23, CenaNetto = 25000.00 };

            //faktura.Produkty = new List<MProdukt> { p, p1 };

            //_accessBase.Save(faktura);


            //var za = new MZamowienie
            //    {
            //        AdresDostawy = new List<MAdres> { adr4, adr3 },
            //        AdresMontazu = new List<MAdres> { adr3 },
            //        DataPodpisania = DateTime.Now,
            //        DataZakonczenia = DateTime.Now,
            //        Klient = os,
            //        Opis = "Zamówienie na meble do sklepu"
            //    };

            //var y = new MPlatnosci { DataZapłaty = DateTime.Now, Wartosc = 250, Zaplacono = false };

            //var y1 = new MPlatnosci { DataZapłaty = DateTime.Now, Wartosc = 251.14, Zaplacono = true };

            //za.Platnosci = new List<MPlatnosci> { y, y1 };

            //za.Specyfikacja = "Specyfikacja";
            //za.WartoscZaliczki = 25.06;
            //za.WartoscZlecenia = y.Wartosc + y1.Wartosc;

            //_accessBase.Save(za);
        }
    }
}
