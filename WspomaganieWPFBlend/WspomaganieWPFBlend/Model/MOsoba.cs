﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System.Collections.Generic;

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MOsoba
    {
        public virtual int Id { get; protected set; }
        public virtual string Imie { get; set; }
        public virtual string Nazwisko { get; set; }
        public virtual string Pseudonim { get; set; }
        public virtual string NrDowodu { get; set; }
        public virtual IList<string> Email { get; set; }
        public virtual IList<string> Telefon { get; set; }
        public virtual string Opis { get; set; }
        public virtual MAdres Adres { get; set; }
        public virtual string Www { get; set; }
        public virtual string Avatar { get; set; }
        public virtual IList<string> Tagi { get; set; }
        public virtual string NazwaPelna { get; set; }
        public virtual string NazwaSkrocona { get; set; }
        public virtual int Nip { get; set; }
        public virtual int Regon { get; set; }
        public virtual bool Firma { get; set; }
    }
}