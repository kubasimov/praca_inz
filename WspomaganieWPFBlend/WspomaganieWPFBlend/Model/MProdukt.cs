﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 
namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MProdukt  
    {
        public virtual int Id { get; set; }
        public virtual string Nazwa { get; set; }
        public virtual double Ilosc { get; set; }
        public virtual double CenaNetto { get; set; }
        public virtual int Vat { get; set; }
    }
}