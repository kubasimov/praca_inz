﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System;
using System.Collections.Generic;

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MFaktura
    {
        public virtual int Id { get; set; }
        public virtual string Numer { get; set; }
        public virtual string Tytulem { get; set; }
        public virtual MOsoba Klient { get; set; }
        public virtual MOsoba Sprzedawca { get; set; }
        public virtual DateTime DataWystawienia { get; set; }
        public virtual DateTime DataSprzedazy { get; set; }
        public virtual double Wartosc { get; set; }
        public virtual IList<MProdukt> Produkty { get; set; }
        public virtual bool Przelew { get; set; }
        public virtual DateTime TerminZaplaty { get; set; }
        
    }
}
//Faktura
//•	Nr faktury
//•	Kopia/Oryginał
//•	Dane klienta
//•	Dane sprzedawcy
//•	Data wystawienia
//•	Data sprzedaży
//•	Produkty (ilość, cena netto, wartość netto, Vat, wartość Vat, cena brutto)
//•	Wartość zamówienia cyfrowo i słownie
//•	Podpis klienta i sprzedawcy
//•	Sposób zapłaty
//•	Termin zapłaty
