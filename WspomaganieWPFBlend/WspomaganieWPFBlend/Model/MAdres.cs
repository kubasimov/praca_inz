﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MAdres
    {
        public virtual int  Id { get; set; }

        public virtual string Ulica { get; set; }

        public virtual string Dom { get; set; }

        public virtual string Lokal { get; set; }

        public virtual string Kod { get; set; }

        public virtual string Miejscowosc { get; set; }

        public virtual MWojewodztwo Wojewodztwo { get; set; }

    }

    
    
}