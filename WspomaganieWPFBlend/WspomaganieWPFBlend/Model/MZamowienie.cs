﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System;
using System.Collections.Generic;

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MZamowienie
    {
        public virtual int Id { get; set; }
        public virtual DateTime DataPodpisania { get; set; }
        public virtual DateTime DataZakonczenia { get; set; }
        public virtual string Opis { get; set; }
        public virtual string Specyfikacja { get; set; }
        public virtual MOsoba Klient { get; set; }
        public virtual IList<MAdres> AdresDostawy { get; set; }
        public virtual IList<MAdres> AdresMontazu { get; set; }
        public virtual double WartoscZlecenia { get; set; }
        public virtual double WartoscZaliczki { get; set; }
        public virtual IList<MPlatnosci> Platnosci { get; set; }

        
    }
}