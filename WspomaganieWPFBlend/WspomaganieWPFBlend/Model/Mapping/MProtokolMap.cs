﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    public class MProtokolMap:ClassMap<MProtokol>
    {
         public MProtokolMap()
         {
             Id(x => x.Id);
             Map(x => x.ZaCo);
             Map(x => x.DatWystawienia);
             Map(x => x.DataPrzyjecia);
             Map(x => x.Uwagi);
         }
    }
}