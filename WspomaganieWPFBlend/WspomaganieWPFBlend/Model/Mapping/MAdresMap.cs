﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    public class MAdresMap:ClassMap<MAdres>
    {
        public MAdresMap()
        {
            Id(x => x.Id);
            Map(x => x.Ulica);
            Map(x => x.Dom);
            Map(x => x.Lokal);
            Map(x => x.Kod);
            Map(x => x.Miejscowosc);
            References(x => x.Wojewodztwo);
        }
    }
}
