﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    class MWojewodztwoMap : ClassMap<MWojewodztwo> 
    {
        public MWojewodztwoMap()
        {
            Id(x => x.Id);
            Map(x => x.Nazwa);
        }
    }
}
