﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    public class MPotwierdzenieMap:ClassMap<MPotwierdzenie>
    {
         public MPotwierdzenieMap()
         {
             Id(x => x.Id);
             Map(x => x.Nazwa);
             References(x => x.Klient);
             Map(x => x.Opis);
             Map(x => x.DataWystawienia);
             Map(x => x.Warotsc);
             Map(x => x.SposobZaplaty);
             Map(x => x.DataWplaty);

         }
    }
}