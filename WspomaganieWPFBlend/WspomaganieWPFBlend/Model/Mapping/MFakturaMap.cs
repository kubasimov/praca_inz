﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    public class MFakturaMap:ClassMap<MFaktura>
    {
         public MFakturaMap()
         {
             Id(x => x.Id);
             Map(x => x.Numer);
             Map(x => x.Tytulem);
             References(x => x.Klient);
             References(x => x.Sprzedawca);
             Map(x => x.DataWystawienia);
             Map(x => x.DataSprzedazy);
             Map(x => x.Wartosc);
             HasMany(x => x.Produkty).Cascade.AllDeleteOrphan();
             Map(x => x.Przelew);
             Map(x => x.TerminZaplaty);
             
         }
    }
}