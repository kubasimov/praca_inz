﻿using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    class MOsobaMap:ClassMap<MOsoba>
    {
        public MOsobaMap()
        {
            Id(x => x.Id).Not.Nullable();
            Map(x => x.Imie);
            Map(x => x.Nazwisko);
            Map(x => x.Pseudonim);
            Map(x => x.NrDowodu);
            HasMany(x => x.Email)
                .Element("Email")
                .Cascade
                .AllDeleteOrphan()
                .Not.LazyLoad();
            HasMany(x => x.Telefon)
                .Element("Telefon")
                .Cascade
                .AllDeleteOrphan()
                .Not.LazyLoad();
            Map(x => x.Opis);
            References(x => x.Adres);
            Map(x => x.Www);
            Map(x => x.Avatar);
            HasMany(x => x.Tagi)
                .Element("Tagi")
                .Cascade
                .AllDeleteOrphan();
            Map(x => x.NazwaPelna);
            Map(x => x.NazwaSkrocona);
            Map(x => x.Nip);
            Map(x => x.Regon);
            Map(x => x.Firma);
        }
    }
}
