﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    public class MZamowienieMap:ClassMap<MZamowienie>
    {
         public MZamowienieMap()
         {
             Id(x => x.Id);
             Map(x => x.DataPodpisania);
             Map(x => x.DataZakonczenia);
             Map(x => x.Opis);
             Map(x => x.Specyfikacja);
             References(x => x.Klient);

             HasMany(x => x.AdresDostawy)
                 .Cascade
                 .AllDeleteOrphan();

             HasMany(x => x.AdresMontazu)
                 .Cascade
                 .AllDeleteOrphan();

             Map(x => x.WartoscZlecenia);
             Map(x => x.WartoscZaliczki);
             HasMany(x => x.Platnosci)
                 .Cascade
                 .AllDeleteOrphan();
         }
    }
}