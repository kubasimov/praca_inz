﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    public class MPlatnosciMap:ClassMap<MPlatnosci>
    {
         public MPlatnosciMap()
         {
             Id(x => x.Id);
             Map(x => x.DataZapłaty);
             Map(x => x.Wartosc);
             Map(x => x.Zaplacono);
         }
    }
}