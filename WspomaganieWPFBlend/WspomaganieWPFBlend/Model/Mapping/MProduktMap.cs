﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Mapping;

namespace Wspomaganie_Obiegu_Dokumentow.Model.Mapping
{
    public class MProduktMap:ClassMap<MProdukt>
    {
         public MProduktMap()
         {
             Id(x => x.Id);
             Map(x => x.Nazwa);
             Map(x => x.Ilosc);
             Map(x => x.CenaNetto);
             Map(x => x.Vat);
         }
    }
}