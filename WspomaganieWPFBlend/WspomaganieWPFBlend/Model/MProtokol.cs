﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System;

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MProtokol
    {
        public virtual int Id { get; set; }
        public virtual string  ZaCo { get; set; }
        public virtual DateTime DatWystawienia { get; set; }
        public virtual DateTime DataPrzyjecia { get; set; }
        public virtual string Uwagi { get; set; }
    }
}

//Protokół (potwierdzenie) odbioru
//•	Cel potwierdzenia
//•	Data wystawienia
//•	Data przyjęcia
//•	Uwagi
//•	Podpisy
