﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MWojewodztwo
    {
        public virtual int Id { get; set; }
        public virtual string Nazwa { get; set; }
    }
}
