﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System;

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MPotwierdzenie
    {
        public virtual int Id { get; set; }
        public virtual string Nazwa { get; set; }
        public virtual MOsoba Klient { get; set; }
        public virtual string Opis { get; set; }
        public virtual DateTime DataWystawienia { get; set; }
        public virtual double Warotsc { get; set; }
        public virtual bool SposobZaplaty { get; set; }
        public virtual DateTime DataWplaty { get; set; }
    }
}

//Potwierdzenie zapłaty
//•	Numer potwierdzenia
//•	Dane wpłacającego
//•	Cel potwierdzenia (czego dotyczy wpłata)
//•	Data wystawienia
//•	Wartość wpłaty
//•	Sposób zaksięgowania
//•	Data wpłaty
//•	Podpis
