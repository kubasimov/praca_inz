﻿// Piotr WachowiczMPlatnosci.csWspomaganieWPFBlend20130806

using System;

namespace Wspomaganie_Obiegu_Dokumentow.Model
{
    public class MPlatnosci
    {
        public virtual int Id { get; set; }
        public virtual DateTime DataZapłaty { get; set; }
        public virtual double Wartosc { get; set; }
        public virtual Boolean Zaplacono { get; set; }
    }
}