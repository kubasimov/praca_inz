﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System.Collections.ObjectModel;
using Wspomaganie_Obiegu_Dokumentow.DataBase;
using Wspomaganie_Obiegu_Dokumentow.Model;

namespace Wspomaganie_Obiegu_Dokumentow.Collections
{
    public class CWojewodztwa : ObservableCollection<MWojewodztwo>
    {
        private readonly AccessBase _ab = new AccessBase();
        
        public CWojewodztwa ()
        {

            #region Load_Data_From_Base_On_Create_class
            
            var woj = _ab.GetListByModel<MWojewodztwo>();
            foreach (var wojewodztwo in woj)
            {
                Add(wojewodztwo);
            }

            #endregion


        }
    }
}
