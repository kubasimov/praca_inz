﻿using System.Windows;

namespace Wspomaganie_Obiegu_Dokumentow.View
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        public Main()
        {
            InitializeComponent();
			
        }

        private void ButtonAdres_Click(object sender, RoutedEventArgs e)
        {
            var a = new VAdres();
            a.ShowDialog();
            
        }

        private void ButtonKlient_Click(object sender, RoutedEventArgs e)
        {
            var a =  new VOsobaFizyczna(1);
            a.ShowDialog();
        }

        
        
    }
}
