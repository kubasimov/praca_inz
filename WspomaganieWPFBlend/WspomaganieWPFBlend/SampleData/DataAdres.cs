﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using Wspomaganie_Obiegu_Dokumentow.Model;

namespace Wspomaganie_Obiegu_Dokumentow.SampleData
{
    class DataAdres
    {
        public MAdres Adres0()
        {
            return new MAdres();
        }
        
        public MAdres Adres1()
        {
            var wojewodztwo = new MWojewodztwo {Nazwa = "Dolnośląskie", Id = 1};

            var adres = new MAdres {Ulica = "Nowolipie", Dom = "26B", Lokal = "16", Kod = "01-011", Miejscowosc = "Warszawa",Wojewodztwo = wojewodztwo};
            return adres;
        }

        public MAdres Adres2()
        {
            var wojewodztwo = new MWojewodztwo {Nazwa = "Warmińsko-mazurskie", Id = 14};

            var adres = new MAdres { Ulica = "Główna", Dom = "25", Kod = "34-234", Miejscowosc = "Warszawa", Wojewodztwo = wojewodztwo };
            //adres.Lokal = "";
            return adres;
        }

        public MAdres Adres3()
        {
            var wojewodztwo = new MWojewodztwo {Nazwa = "Kujawsko-Pomorskie", Id = 2};

            var adres = new MAdres { Ulica = "Prawa", Dom = "2B", Lokal = "36A", Kod = "56-985", Miejscowosc = "Kraków", Wojewodztwo = wojewodztwo };
            return adres;
        }

        public MAdres Adres4()
        {
            var adres = new MAdres{Ulica = "Solidarności",Dom = "2",Lokal = "957",Kod = "31-849",Miejscowosc = "Warszawa"};
            return adres;
        }
    }
}
