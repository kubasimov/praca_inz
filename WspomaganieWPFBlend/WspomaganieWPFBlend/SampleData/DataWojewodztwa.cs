﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System.Collections;

namespace Wspomaganie_Obiegu_Dokumentow.SampleData
{
    class DataWojewodztwa
    {
        public ArrayList Wojewodztwa()
        {
            var wojewodztwa = new ArrayList
                {
                    "Dolnośląskie",
                    "Kujawsko-Pomorskie",
                    "Lubelskie",
                    "Lubuskie",
                    "Łódzkie",
                    "Małopolskie",
                    "Mazowieckie",
                    "Opolskie",
                    "Podkarpackie",
                    "Podlaskie",
                    "Pomorskie",
                    "Śląskie",
                    "Świętokrzyskie",
                    "Warmińsko-mazurskie",
                    "Wielkopolskie",
                    "Zachodniopomorskie"
                };

            return wojewodztwa;
        }
    }
}
