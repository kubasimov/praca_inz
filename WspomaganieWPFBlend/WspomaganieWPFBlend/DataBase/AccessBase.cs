﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace Wspomaganie_Obiegu_Dokumentow.DataBase
{
    
    public class AccessBase
    {
        private readonly AccessDataBase _accessDataBase = new AccessDataBase();
        private readonly ISessionFactory _sessionFactory;

        public AccessBase()
        {
            _sessionFactory = _accessDataBase.OpenConnection();
        }

        /// <summary>
        /// Zapis do bazy
        /// </summary>
        /// <typeparam name="T">Model do zapisania</typeparam>
        /// <param name="name">Nazwa modelu do zapisu</param>
        public void Save<T>(T name)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transacction = session.BeginTransaction())
                {
                    session.Save(name);
                    transacction.Commit();
                }
            }
        }

        /// <summary>
        /// Zapis lub uaktualnienie bazy
        /// </summary>
        /// <typeparam name="T">Model do zapisania</typeparam>
        /// <param name="name">Nazwa modelu do zapisu</param>
        public void SaveOrUpdate<T>(T name)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transacction = session.BeginTransaction())
                {
                    session.SaveOrUpdate(name);
                    transacction.Commit();
                }
            }
        }

        public T GetById<T>(int id)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var xxx = session
                        .CreateCriteria(typeof(T))
                        .Add(Restrictions.Eq("Id", id))
                        .UniqueResult<T>();

                    transaction.Commit();
                    return xxx;
                }
            }
        } 
        
        public  List<T> GetListByModel<T>()
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var xxx = session.Query<T>().ToList();
                    transaction.Commit();

                    return xxx;
                }
            }
        }

        public IList<T> GetListByNameS<T>(string nazwa, string valueS)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var xxx = session
                        .CreateCriteria(typeof (T))
                        .Add(Restrictions.Eq(nazwa, valueS))
                        .List<T>();

                    transaction.Commit();

                    return xxx;
                }
            }
        }

        public IList<T> GetListByNameB<T>(string nazwa, bool valueS)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var xxx = session
                        .CreateCriteria(typeof(T))
                        .Add(Restrictions.Eq(nazwa, valueS))
                        .List<T>();

                    transaction.Commit();

                    return xxx;
                }
            }
        }

        /// <summary>
        /// Pobieranie informacji z bazy
        /// </summary>
        /// <typeparam name="T">Model do pobrania</typeparam>
        /// <param name="nazwa">Nazwa odnosnie ktorej pobieramy dane</param>
        /// <param name="valueS">Wartosc nazwy do porownania</param>
        /// <returns></returns>     
        public T GetByName<T>(string nazwa,string valueS)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var xxx = session
                        .CreateCriteria(typeof (T))
                        .Add(Restrictions.Eq(nazwa, valueS))
                        .UniqueResult<T>();

                    transaction.Commit();

                    return xxx;
                }
            }
        }

        /// <summary>
        /// Pobieranie danych z bazy na podstawie wartosci
        /// </summary>
        /// <typeparam name="T">Model do pobrania</typeparam>
        /// <param name="nazwa">Nazwa odnosnie ktorej pobieramy dane</param>
        /// <param name="valueD">Wartosc nazwy do porownania</param>
        /// <returns></returns>
        public T GetByWartosc<T>(string nazwa, double valueD)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var xxx = session
                        .CreateCriteria(typeof(T))
                        .Add(Restrictions.Eq(nazwa, valueD))
                        .UniqueResult<T>();

                    transaction.Commit();

                    return xxx;
                }
            }
        }

        
    }
}

