﻿// Piotr Wachowicz
// Wspomaganie_Obiegu_Dokumentow
// 2013-08
// 

using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace Wspomaganie_Obiegu_Dokumentow.DataBase
{
    public class AccessDataBase 
    {
        private const string Baza = "Base.sqlite";

        
        private ISessionFactory _sessionFactory;

        public ISessionFactory FirstOpenConnection()
        {
            _sessionFactory = FirstCreateSessionFactory();
            return _sessionFactory;
        }

        public ISessionFactory OpenConnection()
        {
            _sessionFactory = CreateSessionFactory();
            return _sessionFactory;
        }

        public ISessionFactory CloseConnection()
        {
            _sessionFactory.Close();
            return _sessionFactory;
        }
        
        //public List<MWojewodztwo> ReadWojewodztwo()
        //{
        //    using (var session = _sessionFactory.OpenSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            var woj = session.Query<MWojewodztwo>().ToList();
        //            transaction.Commit();

        //            return woj;
        //        }
        //    }
        //}

        //public MAdres[] ReadFromBaseT<T>(string name)
        //{

        //    using (var session = _sessionFactory.OpenSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            //var adres = session
        //            //    .CreateCriteria(typeof(T))
        //            //    .Add(Restrictions.Eq("Ulica", name))
        //            //    .UniqueResult<T>();

        //            var adres = session.Query<MAdres>()
        //                                //.Where(m => m.Ulica == name)
        //                                .Select(m => m).ToArray();

        //            var woj = session.Query<MWojewodztwo>()
        //                             .Select(m => m).ToArray();

        //            var wo1j = session.Query<MWojewodztwo>()
        //                             .Select(m => m).ToFuture();

        //            var woj2 = session.Query<MWojewodztwo>()
        //                             .Select(m => m).ToFutureValue();

        //            var woj3 = session.Query<MWojewodztwo>()
        //                             .Select(m => m).ToList();

        //            var woj4 = session.CreateCriteria(typeof (T)).List<T>();
                    

        //            ObservableCollection<T> dsaw = new ObservableCollection<T>(woj4);
                    
        //            transaction.Commit();
        //            return adres;
        //        }
        //    }


        //}

        //public MAdres ReadFromBase(string name)
        //{
            
        //    using (var session = _sessionFactory.OpenSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            var adres = session
        //                .CreateCriteria(typeof (MAdres))
        //                .Add(Restrictions.Eq("Ulica", name))
        //                .UniqueResult<MAdres>();

        //            transaction.Commit();
        //            return adres;
        //        }
        //    }

            
        //}

        private static ISessionFactory FirstCreateSessionFactory()
        {
            return Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.UsingFile(Baza).ShowSql)
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<AccessDataBase>())
                .ExposeConfiguration(BuildSchema)
                .BuildSessionFactory()
                ;
        }

        private static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.UsingFile(Baza).ShowSql)
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<AccessDataBase>())
                .BuildSessionFactory()
                ;
        }

        private static void BuildSchema(Configuration config)
        {
            new SchemaExport(config).Create(false,true);
        }
    }
}
