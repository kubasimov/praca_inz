﻿namespace ConsoleFNHibernate.Class
{
    public class Osoba
    {
        public virtual int Id { get; protected set; }
        public virtual string Imie { get; set; }
        public virtual string Nazwisko { get; set; }
    }
}