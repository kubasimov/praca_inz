﻿using System.IO;
using ConsoleFNHibernate.Class;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;


namespace ConsoleFNHibernate
{
    class Program
    {
        private const string Baza = "FNHibernate.db";
        
        static void Main()
        {
            var sessionFactory = CreateSessionFactory();

            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var piotr = new Osoba {Imie = "Piotr", Nazwisko = "Kowalski"};
                    var tomasz = new Osoba {Imie = "Tomasz", Nazwisko = "Zablocki"};
                    //var adam = new Osoba {Imie = "Adam", Nazwisko = "Nowak"};

                    session.Save(piotr);
                    session.SaveOrUpdate(tomasz);
                    //session.Save(adam);

                    transaction.Commit();
                }
            }




        }

        private static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.UsingFile(Baza))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Program>())
                .ExposeConfiguration(BuildSchema)
                .BuildSessionFactory();
        }

        private static void BuildSchema(Configuration config)
        {
            //if(File.Exists(Baza))
            //    File.Delete(Baza);

            new SchemaExport(config).Create(false,true);
        }
    }
}
