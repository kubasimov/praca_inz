﻿using ConsoleFNHibernate.Class;
using FluentNHibernate.Mapping;

namespace ConsoleFNHibernate.Mapping
{
    public class OsobaMap : ClassMap<Osoba>
    {
         public OsobaMap()
         {
             Id(x => x.Id);
             Map(x => x.Imie);
             Map(x => x.Nazwisko);
         }
    }
}